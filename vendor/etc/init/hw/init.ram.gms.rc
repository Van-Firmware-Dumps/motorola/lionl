# SPDX-FileCopyrightText: 2016-2023 Unisoc (Shanghai) Technologies Co., Ltd
# SPDX-License-Identifier: LicenseRef-Unisoc-General-1.0

#    All memory configuration as following, which include ZRAM, LMK, RTCC,
# KSM, and some memory properties. The configurations parameters could
# auto-adaptive according to memory size.
#    The Value of property ro.vendor.ramconfig should be {ro.boot.ddrssize} / 256, but if diff
# ro.boot.ddrsize use the same memory configuration parameters, the Value ro.vendor.ramconfig
# should be the same. such as 1024M, 1536M use the same configurations parameters.
#
#    NOTE : IF Add new ro.vendor.ramconfig, PLEASE copy the configurations
#
# between BEGIN line and  END line, Then change the parameters that you want.

#Set LMK swap_free_low_percentage for high_performance_device
on early-init && property:ro.boot.ddrsize=3072M && property:ro.vendor.native_version=
    setprop ro.lmk.swap_free_low_percentage 20
on early-init && property:ro.boot.ddrsize=4096M && property:ro.vendor.native_version=
    setprop ro.lmk.swap_free_low_percentage 20
on early-init && property:ro.boot.ddrsize=6144M && property:ro.vendor.native_version=
    setprop ro.lmk.swap_free_low_percentage 20
on early-init && property:ro.boot.ddrsize=8192M && property:ro.vendor.native_version=
    setprop ro.lmk.swap_free_low_percentage 20

on boot && property:ro.vendor.native_version=
#Disable huge page
    write /sys/kernel/mm/transparent_hugepage/enabled never

# Table ro.boot.ddrsize mapping to ro.vendor.ramconfig
on property:ro.boot.ddrsize=1024M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 4
on property:ro.boot.ddrsize=2048M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 4

on property:ro.boot.ddrsize=3072M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 12
on property:ro.boot.ddrsize=4096M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 12

on property:ro.boot.ddrsize=6144M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 24
on property:ro.boot.ddrsize=8192M && property:ro.vendor.native_version=
    setprop ro.vendor.ramconfig 24

######## ramconfig=4,8 memory parameter configurations BEGIN #######
on property:ro.vendor.ramconfig=4 && property:ro.vendor.native_version=
#Set LMK watermark as {72M,90M,108M,126M,144M,180MB}
    setprop ro.vendor.lmk.adj "0,100,200,250,900,950"
    setprop ro.vendor.lmk.minfree "13824,23040,27648,32256,36864,46080"
    setprop ro.vendor.lmk.customize_minfree true

#Enable watermark_boost on low memory device, default 15,000
     write /proc/sys/vm/watermark_boost_factor 10000

######## ramconfig=4,8 memory parameter configurations END #########


######## ramconfig=12,16 memory parameter configurations BEGIN #######
on property:ro.vendor.ramconfig=12 && property:ro.vendor.native_version=
#Set LMK watermark as {108MB,135MB,162MB,189MB,324MB,472MB}
    setprop ro.vendor.lmk.adj "0,100,200,250,900,950"
    setprop ro.vendor.lmk.minfree "27648,34560,41472,48384,82944,120960"
    setprop ro.vendor.lmk.customize_minfree true

#Disable watermark_boost on low memory device, default 15,000
     write /proc/sys/vm/watermark_boost_factor 0

######## ramconfig=12,16 memory parameter configurations END #########


######## ramconfig=24,32 memory parameter configurations BEGIN #######
on property:ro.vendor.ramconfig=24 && property:ro.vendor.native_version=
#Set LMK watermark as {108MB,135MB,162MB,189MB,324MB,472MB}
    setprop ro.vendor.lmk.adj "0,100,200,250,900,950"
    setprop ro.vendor.lmk.minfree "27648,34560,41472,48384,82944,120960"
    setprop ro.vendor.lmk.customize_minfree true

#follow ums9621_1h10 board
#Enable watermark_boost, use default 15,000
    write /proc/sys/vm/watermark_boost_factor 10000

######## ramconfig=24,32 memory parameter configurations END #########

on property:sys.boot_completed=1 && property:ro.vendor.native_version=
#Enable ZRAM
    write /proc/sys/vm/swappiness 150
    write /proc/sys/vm/page-cluster  0
    write /sys/block/zram0/use_dedup 1
    write /sys/block/zram0/max_comp_streams 7
    write /sys/block/zram0/comp_algorithm   lz4
    swapon_all /vendor/etc/fstab.enableswap

#Enable enhance meminfo only for arm64
    write sys/module/unisoc_mm_emem/parameters/enable 1

#Set watermark_scale
on property:sys.boot_completed=1 && property:ro.vendor.ramconfig=4 && property:ro.vendor.native_version=
    write /proc/sys/vm/watermark_scale_factor 115
    write /proc/sys/vm/min_free_kbytes 10572
on property:sys.boot_completed=1 && property:ro.vendor.ramconfig=12 && property:ro.vendor.native_version=
    write /proc/sys/vm/watermark_scale_factor 140

