#!/vendor/bin/sh
# SPDX-FileCopyrightText: 2016-2023 Unisoc (Shanghai) Technologies Co., Ltd
# SPDX-License-Identifier: LicenseRef-Unisoc-General-1.0

init_value=$(eval "cat /proc/sys/vm/swappiness")

while :
do
        swaptotal=$(eval "cat /proc/meminfo | grep SwapTotal | awk '{print \$2}' ")
        persist_sys_zram_wb_size=persist.sys.zram_wb_size
        wb_prop=`getprop ${persist_sys_zram_wb_size}`
        if [ $swaptotal -ne 0 ]; then
                if [ $wb_prop == 1024M ];then
                    echo 200 > /proc/sys/vm/swappiness
                else
                    echo 185 > /proc/sys/vm/swappiness
                fi
                break
        else
                sleep 2
        fi
done

while :
do
        time=`uptime | awk '{print $3}'`
        if [ $time -lt 6 ]; then
                sleep 10
        else
                echo $init_value > /proc/sys/vm/swappiness
                break
        fi
done

